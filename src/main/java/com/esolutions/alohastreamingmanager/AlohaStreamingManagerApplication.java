package com.esolutions.alohastreamingmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAdminServer
public class AlohaStreamingManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlohaStreamingManagerApplication.class, args);
	}

}
