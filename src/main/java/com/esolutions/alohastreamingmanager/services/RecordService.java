package com.esolutions.alohastreamingmanager.services;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URI;

@Service
public class RecordService {

    public StreamingResponseBody stream(HttpServletResponse response){
        StreamingResponseBody stream = out -> {
            HttpHeaders headers = new HttpHeaders();
            headers.setBasicAuth("user", "pass");
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("user", "pass"));
            restTemplate.execute(
                    URI.create("http://192.168.2.189:9000/stream/raw"),
                    HttpMethod.GET,
                    (ClientHttpRequest request) -> {
                    },
                    responseExtractor -> {
                        response.setContentType("video/flv");
                        IOUtils.copy(responseExtractor.getBody(), response.getOutputStream());
                        response.flushBuffer();
                        return null;
                    }

            );
        };
        return stream;
    }
}
