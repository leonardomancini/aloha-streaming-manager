package com.esolutions.alohastreamingmanager.controllers;

import com.esolutions.alohastreamingmanager.services.RecordService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class RecordController {

    private RecordService recordService;

    public RecordController(RecordService recordService){
        this.recordService = recordService;
    }


    @GetMapping(value = "/stream")
    @ResponseBody
    public ResponseEntity<StreamingResponseBody> process(HttpServletResponse response) throws IOException {
        response.setContentType("video/flv");
       return new ResponseEntity(recordService.stream(response), HttpStatus.OK);

    }
}
